from flask import Flask, request
from parse_rcvd_json import parse_rcvd_json
from message_controller import message_controller
from config_file import *

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def msngr():
    if request.method == 'GET':
        rcvd_token = request.args.get("hub.verify_token")
        return(verify_token(rcvd_token))

    elif request.method == 'POST':
        rcvd_json = request.get_json()
        print(rcvd_json)
        if parse_rcvd_json(rcvd_json) != 'ERR':
            sender = parse_rcvd_json(rcvd_json)[0]
            print('sender',sender)
            if sender != MY_PAGE_ID:
                parsed_rcvd_json = parse_rcvd_json(rcvd_json)
                print(parsed_rcvd_json)
                message_controller(parsed_rcvd_json)
    return('post message processed')

def verify_token(rcvd_token):
    if rcvd_token == VERIFY_TOKEN:
        return request.args.get("hub.challenge")
    return ("token is invalid")

if __name__ == "__main__":
    app.run()