from pymessenger.bot import Bot
from config_file import *


def send_typing(sender):
    bot.send_action(sender, "typing_on")
    print('send typing')
    return('sent_typing')

def send_buttons_carousel_cips(sender):
    bot.send_message(sender, {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [
                    {
                        "title": "connectIPS Information",
                        "image_url": "https://www.retailgazette.co.uk/wp/wp-content/uploads/shutterstock_465600824.jpg",
                        "subtitle": "Pay Direct from Bank!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://connectips.com//",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "https://connectips.com/images/files/cipsfeesandcharges.pdf",
                                "title": "Transaction Charges"
                            }, {
                                "type": "web_url",
                                "url": "https://connectips.com/index.php/transaction-limit",
                                "title": "Transaction Limits"
                            }, {
                                "type": "web_url",
                                "url": "https://connectips.com/index.php/tutorials",
                                "title": "Video Tutorials"
                            }
                        ]

                    },                    {
                        "title": "connectIPS Members",
                        "image_url": "http://blog.teknospire.com/wp-content/uploads/2017/09/Banking-history.jpg",
                        "subtitle": "Pay Direct from Bank!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://connectips.com//",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/ePayment.html",
                                "title": "Member Banks"
                            }, {
                                "type": "web_url",
                                "url": "https://connectips.com/?hash=creditor",
                                "title": "Creditors"
                            }, {
                                "type": "web_url",
                                "url": "https://connectips.com/index.php/component/content/?view=article&id=5&Itemid=120",
                                "title": "Government"
                            }
                        ]

                    },                    {
                        "title": "More..",
                        "image_url": "https://skodaminotti.com/blog/wp-content/uploads/2016/02/Money-Transfer-Online-crop-ID-74171.jpg",
                        "subtitle": "Pay Direct from Bank!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://connectips.com//",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "postback",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD",
                                "title": "Survey"
                            }, {
                                "type": "postback",
                                "title": "Contact Us",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }, {
                                "type": "postback",
                                "title": "Report a problem",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }
                        ]

                    }, {
                        "title": "Services",
                        "image_url": "https://www.workingmediagroup.com/wp-content/uploads/2018/11/money-transfer-3588301_960_720.jpg",
                        "subtitle": "Pay Direct from Bank!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://connectips.com//",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "postback",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD",
                                "title": "NCHL-ECC"
                            }, {
                                "type": "postback",
                                "title": "NCHL-IPS",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }, {
                                "type": "postback",
                                "title": "connectIPS",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }

                        ]

                    }]
            }
        }}
                     )
    print('msg sent')
    return ('msg sent')

def send_buttons_carousel_ips(sender):
    bot.send_message(sender, {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [
                    {
                        "title": "NCHL-IPS Information",
                        "image_url": "https://conversation.which.co.uk/wp-content/uploads/2016/09/Super-complaint-image-convo.png",
                        "subtitle": "Saral Bhuktani ko Madhyam!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://nchl.com.np/informations/view/Interbank-Payments-NCHL-IPS.html",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/direct.html",
                                "title": "NCHL-IPS Members"
                            }, {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/NCHL-IPS-ProductsPurposes.html",
                                "title": "Fees and Charges"
                            }, {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/NCHL-IPS-Timings.html",
                                "title": "NCHL-IPS Timings"
                            }
                        ]

                    }                   ]
            }
        }}
                     )
    print('msg sent')
    return ('msg sent')

def send_buttons_carousel_other(sender):
    bot.send_message(sender, {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [
                    {
                        "title": "Our Services",
                        "image_url": "https://connectips.com/images/banners/nchlbanner.jpg",
                        "subtitle": "Best Financial Services",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://nchl.com.np/",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/Electronic-Cheque-Clearing-NCHL-ECC.html",
                                "title": "NCHL-ECC"
                            }, {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/Interbank-Payments-NCHL-IPS.html",
                                "title": "NCHL-IPS"
                            }, {
                                "type": "web_url",
                                "url": "http://nchl.com.np/informations/view/Overview.html",
                                "title": "About US"
                            }
                        ]

                    },                    {
                        "title": "NCHL Members",
                        "image_url": "https://connectips.com/images/banners/nchlbanner.jpg",
                        "subtitle": "Best Financial Services",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://nchl.com.np/",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/ecc.html",
                                "title": "NCHL-ECC Members"
                            }, {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/direct.html",
                                "title": "NCHL-IPS Members"
                            }, {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/indirect.html",
                                "title": "NCHL-IPS Creditors"
                            }
                        ]

                    },                    {
                        "title": "More..",
                        "image_url": "https://connectips.com/images/banners/nchlbanner.jpg",
                        "subtitle": "Best Financial Services",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://connectips.com//",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "postback",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD",
                                "title": "Survey"
                            }, {
                                "type": "postback",
                                "title": "Contact Us",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }, {
                                "type": "postback",
                                "title": "Report a problem",
                                "payload": "DEVELOPER_DEFINED_PAYLOAD"
                            }
                        ]

                    }]
            }
        }}
                     )
    print('msg sent')
    return ('msg sent')

def send_survey(sender):
        bot.send_message(sender, {
            "text": "Would you like to participate in our survey to help us improve our services?",
            "quick_replies": [
                {
                    "content_type": "text",
                    "title": "I want to participate!",
                    "payload": "<POSTBACK_PAYLOAD>",
                    "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
                }, {
                    "content_type": "text",
                    "title": "No!",
                    "payload": "<POSTBACK_PAYLOAD>",
                    "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
                }
            ]
        })

def send_welcome(sender):
    bot.send_message(sender, {
    "text": "Hi I am NCHL Chat Bot. How can I help you?",
    })

def send_quick_replies_1(sender):
    bot.send_message(sender, {
    "text": "Hi I am NCHL Chat Bot. How can I help you?",
    "quick_replies":[
        {
            "content_type": "text",
            "title": "connectIPS",
            "payload": "<POSTBACK_PAYLOAD>",
            "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
        },

        {
        "content_type":"text",
        "title":"NCHL-ECC",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://connectips.com/templates/protostar/images/loading-screen.png"
      },
      {
        "content_type":"text",
        "title":"NCHL-IPS",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://connectips.com/templates/protostar/images/loading-screen.png"
      },
        {
            "content_type": "text",
            "title": "Stop Bot",
            "payload": "<POSTBACK_PAYLOAD>",
            "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
        }
    ]
  })

def send_quick_replies_2(sender):
    bot.send_message(sender, {
    "text": "Stop the Bot for?",
    "quick_replies":[
      {
        "content_type":"text",
        "title":"24 Hrs",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://connectips.com/templates/protostar/images/loading-screen.png"
      },
      {
        "content_type":"text",
        "title":"1 Week",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://connectips.com/templates/protostar/images/loading-screen.png"
      },
        {
            "content_type": "text",
            "title": "1 Month",
            "payload": "<POSTBACK_PAYLOAD>",
            "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
        }
    ]
  })

def send_quick_replies_survey(sender):
    bot.send_message(sender, {
    "text": "Where did you learn about connectIPS from?",
    "quick_replies":[
      {
        "content_type":"text",
        "title":"Facebook",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://upload.wikimedia.org/wikipedia/en/8/8c/Facebook_Home_logo_old.svg"
      },
      {
        "content_type":"text",
        "title":"Friends",
        "payload":"<POSTBACK_PAYLOAD>",
        "image_url":"https://connectips.com/templates/protostar/images/loading-screen.png"
      },
        {
            "content_type": "text",
            "title": "News",
            "payload": "<POSTBACK_PAYLOAD>",
            "image_url": "https://connectips.com/templates/protostar/images/loading-screen.png"
        }
    ]
  })

def send_quick_reply_contact(sender):
    bot.send_message(sender, {
    "attachment":{
      "type":"template",
      "payload":{
        "template_type":"button",
        "text":"Need further assistance? Talk to a representative",
        "buttons":[
          {
            "type":"phone_number",
            "title":"Call Representative",
            "payload":"+97714255306"
          }
        ]
      }
    }
  })

def send_quick_reply_report(sender):
    bot.send_message(sender, {
    "attachment":{
      "type":"template",
      "payload":{
        "template_type":"button",
        "text":"Please leave your message. We take feedbacks seriously!",
        "buttons":[
          {
            "type":"phone_number",
            "title":"Call Representative",
            "payload":"+97714255306"
          }
        ]
      }
    }
  })

def send_buttons_carousel_ecc(sender):
    bot.send_message(sender, {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [
                    {
                        "title": "NCHL-ECC Information",
                        "image_url": "https://nchl.com.np/uploads/pages/ecc-servic_1542003059.jpg",
                        "subtitle": "Cheques bata Karobar Garau!",
                        "default_action": {
                            "type": "web_url",
                            "url": "https://nchl.com.np/informations/view/Interbank-Payments-NCHL-IPS.html",
                            "webview_height_ratio": "tall",
                        },
                        "buttons": [
                            {
                                "type": "web_url",
                                "url": "http://nchl.com.np/promoters/type/direct.html",
                                "title": "NCHL-ECC Members"
                            }, {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/NCHL-IPS-ProductsPurposes.html",
                                "title": "Fees and Charges"
                            }, {
                                "type": "web_url",
                                "url": "https://nchl.com.np/informations/view/NCHL-IPS-Timings.html",
                                "title": "NCHL-ECC Timings"
                            }
                        ]

                    }                   ]
            }
        }}
                     )
    print('msg sent')
    return ('msg sent')
